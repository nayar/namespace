package namespace

import (
	"bitbucket.org/nayar/pubsub"
	"reflect"
	"strings"
	"sync"
	"time"
)

type Namespace struct {
	lock   sync.RWMutex
	prefix string
	ps     *pubsub.Pubsub
	data   map[string]interface{}
}

type WaitF func(*Namespace) bool

var defaultInstance *Namespace
var moduleLock sync.Mutex

func DefaultInstance() *Namespace {
	moduleLock.Lock()
	defer moduleLock.Unlock()
	if defaultInstance == nil {
		defaultInstance = New()
	}
	return defaultInstance
}

func New() *Namespace {
	ns := new(Namespace)
	ns.ps = pubsub.DefaultInstance()
	ns.prefix = "r._ns." + pubsub.GetUidStr() + "."
	ns.data = make(map[string]interface{})
	return ns
}

func (ns *Namespace) SetPs(ps *pubsub.Pubsub) *Namespace {
	ns.ps = ps
	return ns
}

func (ns *Namespace) Ps() *pubsub.Pubsub { return ns.ps }

func (ns *Namespace) SetPrefix(prefix string) *Namespace {
	ns.prefix = prefix
	if !strings.HasSuffix(ns.prefix, ".") {
		ns.prefix += "."
	}
	return ns
}

func (ns *Namespace) Prefix() string { return ns.prefix }

func (ns *Namespace) GetTree(prefix string) (m map[string]interface{}) {
	if prefix != "" && !strings.HasSuffix(prefix, ".") {
		prefix += "."
	}
	m = make(map[string]interface{})
	ns.lock.RLock()
	defer ns.lock.RUnlock()
	for k, v := range ns.data {
		if strings.HasPrefix(k, prefix) {
			m[k[len(prefix):]] = v
		}
	}
	return m
}

func (ns *Namespace) SetTree(prefix string, m map[string]interface{}) {
	if prefix != "" && !strings.HasSuffix(prefix, ".") {
		prefix += "."
	}
	for k, v := range m {
		ns.Set(prefix+k, v)
	}
}

func (ns *Namespace) Clear() {
	ns.lock.Lock()
	defer ns.lock.Unlock()
	ns.data = make(map[string]interface{})

}

func (ns *Namespace) AddPrefix(key string) string {
	return ns.prefix + key
}

func (ns *Namespace) AddPrefixMany(keys []string) (r []string) {
	for _, key := range keys {
		r = append(r, ns.prefix+key)
	}
	return
}

func (ns *Namespace) Wait(f WaitF, t time.Duration, keys ...string) (ok bool) {
	var delay time.Duration

	if ok = f(ns); !ok {
		s := ns.ps.NewSubscriber()
		s.Subscribe(ns.AddPrefixMany(keys)...)
		defer s.Clear()
		now := time.Now()
		for {
			if ok = f(ns); ok {
				break
			}
			if t <= 0 {
				delay = 0
			} else {
				if delay = t - time.Since(now); delay <= 0 {
					break
				}

			}
			if _, ch := s.Wait(delay); !ch {
				break
			}
		}
	}
	return
}

func (ns *Namespace) Get(k string, d ...interface{}) interface{} {
	ns.lock.RLock()
	defer ns.lock.RUnlock()
	r, ok := ns.data[k]
	if !ok && len(d) > 0 {
		r = d[0]
	}
	return r
}

func (ns *Namespace) Set(k string, v interface{}) {
	ns.lock.Lock()
	defer ns.lock.Unlock()
	if !reflect.DeepEqual(ns.data[k], v) {
		ns.data[k] = v
		ns.ps.Send(&pubsub.Message{To: ns.prefix + k, Val: v})
	}
}

func (ns *Namespace) SetF(k string, v interface{}) {
	ns.lock.Lock()
	defer ns.lock.Unlock()
	ns.data[k] = v
	ns.ps.Send(&pubsub.Message{To: ns.prefix + k, Val: v})
}

func (ns *Namespace) Delete(key string) {
	ns.lock.Lock()
	defer ns.lock.Unlock()
	delete(ns.data, key)
}

func (ns *Namespace) Exists(key string) (ok bool) {
	ns.lock.RLock()
	defer ns.lock.RUnlock()
	_, ok = ns.data[key]
	return
}
