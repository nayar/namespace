package namespace

import (
	"testing"
	"time"
)

func TestDefaultInstance(t *testing.T) {
	ns := DefaultInstance()
	if ns == nil {
		t.Errorf("No default instance")
	}
}

func TestSetGetClear(t *testing.T) {
	ns := DefaultInstance()
	ns.Set("int", 5)
	ns.Set("float", 5.5)
	ns.Set("string", "Hola mundo")
	ns.Set("bool", true)

	if ns.Get("int") != 5 {
		t.Errorf("GetInt fail")
	}
	if ns.Get("float") != 5.5 {
		t.Errorf("GetFloat fail")
	}
	if ns.Get("string") != "Hola mundo" {
		t.Errorf("GetString fail")
	}
	if ns.Get("bool") != true {
		t.Errorf("GetBool fail")
	}
	ns.Clear()
	if ns.Get("int", -10) != -10 {
		t.Errorf("Namespace not cleared")
	}
}

func TestWait(t *testing.T) {
	ns := DefaultInstance()
	ns.Clear()
	go func() {
		time.Sleep(10 * time.Millisecond)
		ns.Set("int", 10)
		time.Sleep(10 * time.Millisecond)
		ns.Set("int", 1)
		return
	}()
	var f WaitF
	f = func(n *Namespace) bool { return n.Get("int") == 10 }
	if !ns.Wait(f, 20*time.Millisecond, "int") {
		t.Errorf("Wait Equal Failed")
	}
	f = func(n *Namespace) bool { return n.Get("int") != 10 }
	if !ns.Wait(f, 20*time.Millisecond, "int") {
		t.Errorf("Wait not Equal Failed")
	}
}

func TestTree(t *testing.T) {
	ns := DefaultInstance()
	ns.Set("root.cards.as", 1)
	ns.Set("root.cards.sota", 10)
	ns.Set("root.cards.caballo", 11)
	ns.Set("root.cards.rey", 12)
	tree := ns.GetTree("root.cards")
	if _, ok := tree["sota"]; !ok {
		t.Errorf("Key not found after GetTree")
	}
	ns.SetTree("root.naipes", tree)
	if ns.Get("root.naipes.sota") != 10 {
		t.Errorf("Key not found after SetTree")
	}
}
